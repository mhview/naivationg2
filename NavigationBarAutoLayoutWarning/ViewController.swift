//
//  ViewController.swift
//  NavigationBarAutoLayoutWarning
//
//  Created by Leontien Balkenende on 07/09/2019.
//  Copyright © 2019 Tientijd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nc = segue.destination as? UINavigationController
        if let rvc = nc?.viewControllers.first {
            rvc.navigationItem.title = "This is the title"
        }
    }
}

